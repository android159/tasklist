package com.example.myapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


//ExpandableListView
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button addTask, removeTasks, deleteCheckedTasks;
    TextView dateTimeDisplay, editing;
    ListView newTask;
    final Context context = this;
    final ArrayList<Task> task = new ArrayList<Task>();
    //адаптер для listview задач
    DBHelper dbHelper;
    TaskMainAdapter taskMainAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //создаём экземпляр бд
        dbHelper = new DBHelper(this);

        newTask = (ListView) findViewById(R.id.listTask);

        //метод для заполнения listview при включение приложения
        loadTaskInListView();

        //показ дня
        getCurrentDate();

        //понять, что нажали на задания в списке
        editing = (TextView) findViewById(R.id.list_view_task_text);

        //работа с кнопками
        addTask = findViewById(R.id.add_task);
        removeTasks = findViewById(R.id.delete_task);
        deleteCheckedTasks = findViewById(R.id.delete_checked_task);
        addTask.setOnClickListener(this);
        removeTasks.setOnClickListener(this);
        deleteCheckedTasks.setOnClickListener(this);
    }

    //показ дня
    private void getCurrentDate() {
        Locale localeRu = new Locale("ru", "RU");
        dateTimeDisplay = (TextView) findViewById(R.id.calendar_date);
        Date dateNow = new Date();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("EEEE, dd.MM.yyyy", localeRu);
        dateTimeDisplay.setText(formatForDateNow.format(dateNow));
    }

    //загружаем все задачи в listView
    private void loadTaskInListView() {
        //предназачен для управления бд - SQLiteDatabase
        final SQLiteDatabase database = dbHelper.getWritableDatabase();

        Cursor cursor = database.query(DBHelper.TABLE_TASK, null, null, null, null, null, null);

        taskMainAdapter = new TaskMainAdapter(this, task);
        this.taskMainAdapter.clear();
        this.taskMainAdapter.notifyDataSetChanged();
        newTask.setAdapter(taskMainAdapter);

        if (cursor.moveToFirst()) {
            int nameIndex = cursor.getColumnIndex(DBHelper.KEY_TASK_NAME);

            do {
                task.add(new Task(cursor.getString(nameIndex), false));
            } while (cursor.moveToNext());

            taskMainAdapter = new TaskMainAdapter(this, task);
            newTask.setAdapter(taskMainAdapter);
        }
        cursor.close();
    }

    //работа с меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        //создаём меню
        menu.add(2, 4, 4, "item3");
        return true;
    }

    //меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    //слушатель нажатия на кнопки
    @Override
    public void onClick(View v) {

        //предназачен для управления бд - SQLiteDatabase
        final SQLiteDatabase database = dbHelper.getWritableDatabase();

        //используется для добавления новых строк в таблицу
        final ContentValues contentValues = new ContentValues();
        LayoutInflater li = LayoutInflater.from(context);
        //Создаем AlertDialog
        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(context);
        switch (v.getId()) {
            case R.id.add_task:
                //Получаем вид с файла prompt.xml, который применим для диалогового окна:
                View promptsView = li.inflate(R.layout.dialog_window_add_task, null);
                //Настраиваем dialog_window_add_task.xml для нашего AlertDialog:
                mDialogBuilder.setView(promptsView);
                //Настраиваем отображение поля для ввода текста в открытом диалоге:
                final EditText userInput = (EditText) promptsView.findViewById(R.id.input_text);
                //Настраиваем сообщение в диалоговом окне:
                mDialogBuilder
//                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //удаляем \n
                                        String text = userInput.getText().toString().replaceAll("\r", "").replaceAll("\n", "");
                                        //добавляю значение в объект contentValues
                                        if (text != null && !text.isEmpty()) {
                                            String query = "SELECT " + DBHelper.KEY_TASK_NAME + " FROM " + DBHelper.TABLE_TASK + " WHERE " + DBHelper.KEY_TASK_NAME + "= '"+ text + "'";
                                            if (database.rawQuery(query, null).getCount() > 0) {
                                                Toast toast = Toast.makeText(MainActivity.this, "Задание с этим именем уже существует", Toast.LENGTH_LONG);
                                                toast.show();
                                            } else {
                                                contentValues.put(DBHelper.KEY_TASK_NAME, text);
                                                //сохраняю в бд
                                                database.insert(DBHelper.TABLE_TASK, null, contentValues);
                                                dbHelper.close();
                                                loadTaskInListView();
                                            }
                                        } else {
                                            Toast toast = Toast.makeText(MainActivity.this, "Задание без названия", Toast.LENGTH_LONG);
                                            toast.show();
                                        }
                                    }
                                })
                        .setNegativeButton("Отмена",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                //Создаем AlertDialog:
                AlertDialog alertDialog = mDialogBuilder.create();
                //и отображаем его:
                alertDialog.show();

//            //высплывающий текст
//            Toast toast = Toast.makeText(MainActivity.this, "Нажата кнопка", Toast.LENGTH_LONG);
//            toast.show();
                break;
            case R.id.delete_task:
//                mDialogBuilder = new AlertDialog.Builder(context);
                mDialogBuilder.setTitle("Удаление");  // заголовок
                mDialogBuilder.setMessage("Удалить все задания?"); // сообщение
                mDialogBuilder.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        database.delete(DBHelper.TABLE_TASK, null, null);
                        dbHelper.close();
                        loadTaskInListView();
                    }
                }).setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                    }
                });
                mDialogBuilder.show();
                break;
            case R.id.delete_checked_task:
//                mDialogBuilder = new AlertDialog.Builder(context);
                mDialogBuilder.setTitle("Удаление");  // заголовок
                mDialogBuilder.setView(li.inflate(R.layout.editable_window, null));
                mDialogBuilder.setMessage("Удалить выбранные задания?"); // сообщение
                mDialogBuilder.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        String deleteTasks = taskMainAdapter.deleteCheckedTask();
                        database.delete(DBHelper.TABLE_TASK, DBHelper.KEY_TASK_NAME + " IN(" + deleteTasks + ")", null);
                        loadTaskInListView();
                    }
                }).setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                    }
                });
                mDialogBuilder.show();
                break;
            case R.id.list_view_task_text:
                //создаю новый view layout чтобы прокинуть в него значения и затем засетить в диалог
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.editable_window,null);
                TextView text = (TextView)layout.findViewById(R.id.task_name_in_edit_window);
                text.setText(((AppCompatTextView) v).getText());
                mDialogBuilder.setTitle("Редактирование задачи");  // заголовок
                mDialogBuilder.setView(layout);
                mDialogBuilder.setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                    }
                }).setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                    }
                });
                mDialogBuilder.show();
                break;
            default:
                break;
        }
    }
}