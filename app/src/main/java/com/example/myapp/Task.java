package com.example.myapp;

public class Task {
    private String taskName;
    private boolean box;

    Task(String taskName, boolean box) {
        setTaskName(taskName);
        setBox(box);
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskName() {
        return taskName;
    }

    public boolean isBox() {
        return box;
    }

    public void setBox(boolean box) {
        this.box = box;
    }
}
