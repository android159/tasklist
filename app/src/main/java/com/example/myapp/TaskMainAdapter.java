package com.example.myapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

public class TaskMainAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Task> objects;

    public TaskMainAdapter(Context context, ArrayList<Task> tasks) {
        ctx = context;
        objects = tasks;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void clear() {
        objects.clear();
    }

    public String deleteCheckedTask() {
        String join = "";
        for (Task task : objects) {
            if (task.isBox()) {
                join += DBHelper.KEY_TASK_NAME + " = " + task.getTaskName() + " AND";
            }
        }
        if (join.isEmpty()) {
            return "";
        } else {
            return join.substring(0, join.length() - 4);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.task_list_view_menu_with_checkbox, parent, false);
        }

        Task t = getTask(position);

        ((TextView) view.findViewById(R.id.list_view_task_text)).setText(t.getTaskName());

        CheckBox cbBuy = (CheckBox) view.findViewById(R.id.list_view_task_checkbox);
        // присваиваем чекбоксу обработчик
        cbBuy.setOnCheckedChangeListener(myCheckChangeList);
        // пишем позицию
        cbBuy.setTag(position);
        return view;
    }

    Task getTask(int position) {
        return ((Task) getItem(position));
    }

    // обработчик для чекбоксов
    CompoundButton.OnCheckedChangeListener myCheckChangeList = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            // меняем данные задачи(выбранна или нет)
            getTask((Integer) buttonView.getTag()).setBox(isChecked);
        }
    };
}