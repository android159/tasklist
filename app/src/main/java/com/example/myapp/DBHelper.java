package com.example.myapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    //версия бд
   public static final int DATABASE_VERSION = 1;
   public static final String DATABASE_NAME = "taskList";
   public static final String TABLE_TASK = "task";

   //с нижним подчёркиванием, для работы с курсором
   public static final String KEY_ID = "_id";
   public static final String KEY_TASK_NAME = "_taskName";

    //обязательно вызываем конструктор и передаем параметры предку
    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //вызывается при создание бд
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + TABLE_TASK + "(" + KEY_ID + " integer primary key, " + KEY_TASK_NAME + " text)");
    }

    //вызывается при изменение бд
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exists " + TABLE_TASK);

        onCreate(sqLiteDatabase);
    }
}